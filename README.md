# Worldline Assessment 4

This doc is having details of the submission .


# Puppet and Terraform Integration

## Requirements:
- terraform setup done
- aws cli logged in 

### 1. Clone GitLab Repository:

- git clone https://gitlab.com/mortal007/puppetterraformintegration.git
- cd PuppetTerraformIntegration

### 2. Infrastructure Provisioning with Terraform:

- go into the terraform folder 

 ` cd terraform `
- run following commands   
  ` terraform init `   
  ` terraform plan `   
  ` terraform apply `

### 3. Configuration Management with Puppet:
- check the manifests in puppet folder
- below is main.tf file provisioning aws instance 

```hcl

provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "myagent" {
  ami           = "ami-0440d3b780d96b29d"
  instance_type = "t2.micro"
  key_name      = "mortalec2key"

  tags = {
    Name = "myagent"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("mortalec2key.pem")
    host        = self.public_ip
    timeout     = "5m" 
  }


  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"172.31.29.138 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test"
    ]
  }

}

output "public_ip" {
  value = aws_instance.myagent.public_ip
}

##note that 172.31.29.138 is my masters private ip address


```
using terraform provisioners copying manifests and executing scripts stored in apply_manifests.sh

